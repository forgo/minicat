package org.coody.framework.minicat.builder;

import java.io.IOException;
import java.net.Socket;

import org.coody.framework.minicat.builder.iface.HttpBuilder;
import org.coody.framework.minicat.entity.HttpServletRequest;
import org.coody.framework.minicat.exception.NotConnectionException;
import org.coody.framework.minicat.util.StringUtil;

public class BioHttpBuilder extends HttpBuilder{

	private Socket socket;
	
	public BioHttpBuilder(Socket socket){
		if(socket==null){
			throw new NotConnectionException("未实例化Socket");
		}
		this.socket=socket;
	}
	@Override
	protected void buildRequest() throws Exception {
		this.request = new HttpServletRequest(socket.getInputStream());
	}

	@Override
	protected void flush() throws IOException {
		byte[] data = response.getOutputStream().toByteArray();
		if (StringUtil.isNullOrEmpty(data)) {
			return;
		}
		socket.getOutputStream().write(data);
	}


}
